// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

$(document).ready(function(){

    group_handler();

    audit_handler();

    get_graph_data();

});

function get_graph_data(){

    var chart = $('.chart');

    if(chart.length > 0){

        $.get(
            '/graphData',
            function(data){
                display_graph(data);
            }
        );

    }

}

function display_graph(data){

    var audits = data.audits,
        emp_types = [],
        data_points = [],
        series = [],
        color_map = [
            'rgba(119, 152, 191, .5)',
            'rgba(223, 83, 83, .5)'
        ];

    for(i=0;i<audits.length;i++){

        var audit = audits[i];

        // add our data points
        var point_data = {},
            risk_rating,
            hours,
            errors = false;


        switch(audit.risk_rating){
            case "low":
                risk_rating = -100;
                break;
            case "limited":
                risk_rating = 50;
                break;
            case "moderate":
                risk_rating = 0;
                break;
            case "considerable":
                risk_rating = 50;
                break;
            case "high":
                risk_rating = 100;
                break;
            default:
                risk_rating = null;
                errors = true;
        }

        if(audit.hours_over != 0){
            hours = audit.hours_over;
        } else if(audit.hours_under != 0){
            hours = audit.hours_under * -1;
        } else {
            hours = 0;
        }

        console.log(audit);

        if(
            audit.emp_group === "assign group" ||
            audit.emp_level === "assign group" ||
            audit.hours_over === "assign group" ||
            audit.hours_under === "assign group" ||
            audit.in_range === "assign group" ||
            audit.total_hours === "assign group"
        ){
            errors = true;
        }

        // data must be an array, x val first
        if(!errors){

            var add = true,
                data = [risk_rating, hours],
                pos;

            for(e=0;e<emp_types.length;e++){
                var type = emp_types[e];
                if(type.hasOwnProperty('name') && type.name === audit.emp_group){
                    pos = e;
                    add = false;
                }
            }

            if(add){
                var typeData = {
                    name: audit.emp_group,
                    val: [],
                    color: color_map[pos]
                };

                pos = emp_types.length;
                emp_types.push({name: audit.emp_group, val: [], color: color_map[pos]});
            }

            emp_types[pos].val.push(data);

        }

    }

    for(i=0; i<emp_types.length; i++){
        var type = emp_types[i];

        series_data = {
            name: type.name,
            color: type.color,
            data: type.val
        };

        series.push(series_data);
    }

    $('.chart').highcharts({
        chart: {
            type: 'scatter',
            zoomType: 'xy'
        },
        title: {
            text: 'Total hours per audit, Chicago Fed'
        },
        xAxis: {
            title: {
                enabled: true,
                text: 'Risk Level'
            },
            startOnTick: true,
            endOnTick: true,
            showLastLabel: true,
            ceiling: 100,
            floor: -100,
            max: 100,
            min: -100,
            tickInterval: 50,
            labels: {
                formatter: function(){
                    switch(this.value){
                        case -100:
                            return "Low";
                            break;
                        case -50:
                            return "Limited";
                            break;
                        case 0:
                            return "Moderate";
                            break;
                        case 50:
                            return "Considerable";
                            break;
                        case 100:
                            return "High";
                            break;
                    }

                    return this.value;
                }
            }
        },
        yAxis: {
            title: {
                text: 'Total Hours per Audit'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 100,
            y: 70,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF',
            borderWidth: 1
        },
        plotOptions: {
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br>',
                    pointFormat: '{point.y} hours against average'
                }
            }
        },
        series: series
    });

}

function group_handler(){

  $('#new-group').click(function(){

    var name = $('#group-name').val(),
        level = parseInt($('#group-level').val(),10),
        low = parseInt($('#group-hours-low').val(),10),
        high = parseInt($('#group-hours-high').val(),10),
        errors = false;

    $('.error-container').empty();

    if(name === ""){
        $('.error-container.name').text('Type cannot be empty');
        errors = true;
    }

    if(level < 1 || level > 5){
        $('.error-container.level').text('Invalid level');
        errors = true;
    }

    if(low < 0){
        $('.error-container.hours-low').text('Hours cannot be negative');
        errors = true;
    }

    if(high < 0){
        $('.error-container.hours-high').text('Hours cannot be negative');
        errors = true;
    }

    if(high < low){
        $('.error-container.hours-high').text('High hours must be less than low hours');
        errors = true;
    }

    var id = $('#existing').text(),
        data = {
            emp_type: name,
            emp_level: level,
            hours_low: low,
            hours_high: high
        },
        url = "/group";

    if(id != 0 && id != "0"){
        url = "/group/" + id.trim();
    }

    if(!errors){
        $.post(
            url,
            data,
            function(){
                window.location = "/groups"
            }
        ).fail(function(res){
           $('.general-error').text('An error occurred');
           console.log(res);
        });
    }

  });

  $('#remove-group').click(function(){

        var id = $(this).data('id');

        $.ajax({
            url: '/group',
            type: 'DELETE',
            data: {id: id}
        }).success(function(){
            window.location = "/groups";
        }).error(function(res){
            $('.general-error').text('An error occurred');
            console.log(res);
        });
  });

}

function audit_handler(){

  $.get(
      "/groups/types",
      populateEmpType
  );

  // show unassigned groups
  var fields = $('.check-for-errors');

  for(i=0;i<fields.length;i++){
    var field = fields[i],
        jField = $(field),
        text = jField.text(),
        trimmed = text.trim();

    if(trimmed === "assign group"){
      jField.addClass('alert');
      jField.closest('tr').addClass('alert');


    }
  }

  $('#new-audit').click(function(){

    var name = $('#inst-name').val(),
        date = parseInt($('#audit-date').val(),10),
        emp_type = $('#emp-type').val(),
        offsite = parseInt($('#offsite-hours').val(),10),
        onsite = parseInt($('#onsite-hours').val(),10),
        risk_rating = $('#risk-rating').val(),
        audit_date = $('#audit-date').val(),
        errors = false;

    $('.error-container').empty();

    if(name === ""){
        $('.error-container.name').text('Type cannot be empty');
        errors = true;
    }

    if(date === ""){
        $('.error-container.date').text('Invalid level');
        errors = true;
    }

    if(offsite < 0){
        $('.error-container.offsite-hours').text('Hours cannot be negative');
        errors = true;
    }

    if(onsite < 0){
        $('.error-container.onsite-hours').text('Hours cannot be negative');
        errors = true;
    }

    var id = $('#existing').text(),
        data = {
            inst_name: name,
            emp_type: emp_type,
            offsite_hours: offsite,
            onsite_hours: onsite,
            audit_date: date,
            risk_rating: risk_rating,
            audit_date: audit_date
        },
        url = "/audit";

    if(id != 0 && id != "0"){
        url = "/audit/" + id.trim();
    }

    if(!errors){
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success: function(){
                window.location = "/"
            },
            error: function(res){
               $('.general-error').text('An error occurred');
               console.log(res);
            }
        });
    }

  });

  $('#remove-audit').click(function(){

        var id = $(this).data('id');

        $.ajax({
            url: '/audit',
            type: 'DELETE',
            data: {id: id}
        }).success(function(){
            window.location = "/";
        }).error(function(res){
            $('.general-error').text('An error occurred');
            console.log(res);
        });
  });


}

function populateEmpType(items){
    var select_el = $('#emp-type'),
        types = items.types,
        emp_type_id = $('#type-id').text(),
        ids_array = _.pluck(types, 'id');

    for(i=0;i<types.length;i++){
        var option = '<option value="' +  types[i].id+ '">' + types[i].label + '</option>';
        select_el.append(option);
    }



    if(emp_type_id !== "null"){
        if(_.indexOf(ids_array, emp_type_id > -1)){
            var active_option = select_el.find('option[value="' + emp_type_id + '"]');
            active_option.attr('selected','selected');
        } else {
            select_el.addClass('warning');
            $('.error-container.emp-type').text('The group that is selected no longer exists. It will be updated when this records is saved');
        }
    }
}
