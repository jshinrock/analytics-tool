/**
* Audit.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    emp_type: {
      collection: "group",
      via: "emp_present"
    },
    inst_name: {
        type: "string",
        required: true
    },
    offsite_hours: {
      type: "int"
    },
    risk_rating: {
        type: "string"
    },
    onsite_hours: {
      type: "int"
    },
    audit_date: {
      type: "string",
      required: true
    },

    parseAuditData: function() {

        var high_range = null,
            low_range = null;

/*
        Group.findOne().exec(function(err, group){
            if(err){
                return err;
            } else if(group){

                high_range = group.hours_high;
                low_range = group.hours_low;
                this.in_range = "No";
                this.total_hours = parseInt(this.offsite_hours) + parseInt(this.onsite_hours);

                if(this.total_hours <= hours_low && this.total_hours >= hours_high){
                    this.in_range = "Yes";
                }


            } else {
                return {err: "Cannot find group"}
            }
        });
*/

    }
  }
};

