/**
* Group.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,

  attributes: {
    emp_type: {
      type: "String",
      required: true
    },
    emp_level: {
      type: "int",
      required: true
    },
    hours_low: {
      type: "int",
      required: true
    },
    hours_high: {
      type: "int",
      required: true
    },
    emp_present: {
      collection: "audit",
      via: "emp_type"
    }
  }
};

