/**
 * GroupController
 *
 * @description :: Server-side logic for managing groups
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

    allGroups: function(req, res){

        Group.find().exec(function(err, groups){

          if(err){
            console.log(err);
            res.json({err: err});
          } else {

            res.view('foundation/groups', {
              groups: groups
            });

          }

        });

    },

    editGroup: function(req, res){

        if(_.isEmpty(req.param('id'))){
            res.view('foundation/error',{
                error: error
            });
        } else {

            var id = req.param('id');

                Group.findOne({id:id}).exec(function(err, group){

                if(err){
                    res.json({err: err});
                } else if(group){
                    res.view('foundation/add_group', {
                      group: group
                    });
                } else {
                    res.view('foundation/error',{
                      error: "Record not found"
                    });
                }
              });
      }
    },

    confirmRemoveGroup: function(req, res){

        if(_.isEmpty(req.param('id'))){
          res.view('analytics/error',{
              error: error
          });
        } else {

            var id = req.param('id');

            Group.findOne({id:id}).exec(function(err, group){

            if(err){
                 res.json({err: err});
            } else if(group){
                res.view('foundation/remove_group', {
                    group: group
                });
            } else {
                res.view('foundation/error',{
                    error: "Record not found"
                });
             }
          });
        }

  },

  getTypes: function(req, res){

    Group.find().exec(function(err, groups){
        if(err){
            res.json({err: err});
        } else {

            var types = [];

            _.each(groups, function(group){
                var label = group.emp_type + ":" + group.emp_level;
                types.push({label: label, id: group.id});
            });

            res.json({types: types});

        }
    });

  }

};

