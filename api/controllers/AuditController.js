/**
 * AuditController
 *
 * @description :: Server-side logic for managing audits
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

	mainView: function(req, res){

      	Audit.find().populate('emp_type').exec(function(err, audits){

            if(err){
                console.log(err);
                res.json({err: err});
            } else {

                _.each(audits, function(audit){

                    if(_.has(audit, 'emp_type') && !_.isEmpty(audit.emp_type)){
                        var emp_type = audit.emp_type[0];

                        audit = AuditService.parse(audit, emp_type);
                    } else {
                        audit = AuditService.displayUndefined(audit);
                    }

                });

                return res.view('foundation/index', {
                    audits: audits
                });

            }

      	});
	},

	getGraphData: function(req, res){

        Audit.find().populate('emp_type').exec(function(err, audits){

            if(err){
                console.log(err);
                res.json({err: err});
            } else {

                _.each(audits, function(audit){

                    if(_.has(audit, 'emp_type') && !_.isEmpty(audit.emp_type)){

                        var emp_type = audit.emp_type[0],

                        audit = AuditService.parse(audit, emp_type);

                    } else {

                        audit = AuditService.displayUndefined(audit);

                    }

                });


                res.json(200, {audits: audits});

            }

        });

	},

	editAudit: function(req, res){

        if(_.isEmpty(req.param('id'))){
            res.view('foundation/error',{
                error: error
            });
        } else {

            var id = req.param('id');

                Audit.findOne({id:id}).populate('emp_type').exec(function(err, audit){

                if(err){
                    res.json({err: err});
                } else if(audit){

                    if(_.has(audit, 'emp_type') && !_.isEmpty(audit.emp_type)){

                       var emp_type = audit.emp_type[0];

                        audit.emp_type_id = emp_type.id;

                    } else {

                        audit.emp_type_id = 0

                    }

                    res.view('analytics/add_audit', {
                      audit: audit
                    });
                } else {
                    res.view('analytics/error',{
                      error: "Record not found"
                    });
                }
            });

      }

	},

    confirmRemoveAudit: function(req, res){

        if(_.isEmpty(req.param('id'))){
          res.view('foundation/error',{
              error: error
          });
        } else {

            var id = req.param('id');

            Audit.findOne({id:id}).populate('emp_type').exec(function(err, audit){

            if(err){
                 res.json({err: err});
            } else if(audit){

                if(_.has(audit, 'emp_type') && !_.isEmpty(audit.emp_type)){

                    var emp_type = audit.emp_type[0],

                    audit = AuditService.parse(audit, emp_type);

                } else {

                    audit = AuditService.displayUndefined(audit);

                }

                res.view('foundation/remove_audit', {
                    audit: audit
                });
            } else {
                res.view('foundation/error',{
                    error: "Record not found"
                });
             }
          });
        }

  },

};

