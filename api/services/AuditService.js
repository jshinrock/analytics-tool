module.exports = {

    parse: function(audit, emp_type) {

        var emp_name = emp_type.emp_type,
            emp_level = emp_type.emp_level,
            low_hours = emp_type.hours_low,
            high_hours = emp_type.hours_high,
            total_hours = parseInt(audit.onsite_hours) + parseInt(audit.offsite_hours);

        audit.emp_group = emp_name;
        audit.emp_level = emp_level;
        audit.in_range = "No";
        audit.total_hours = total_hours;
        audit.hours_over = 0;
        audit.hours_under = 0;


        if(total_hours >= low_hours && total_hours <= high_hours){
            audit.in_range = "Yes";
        }

        if(total_hours < low_hours){
            audit.hours_under = low_hours - audit.total_hours;
        }

        if(total_hours > high_hours){
            audit.hours_under = audit.total_hours - high_hours;
        }

        return audit;

    },

    displayUndefined: function(audit) {

        audit.emp_group = "assign group";
        audit.emp_level = "assign group";
        audit.in_range = "assign group";
        audit.total_hours = "assign group";
        audit.hours_over = "assign group";
        audit.hours_under = "assign group";

        return audit;

    }
};
