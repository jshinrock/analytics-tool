// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

$(document).ready(function(){

  // Groups section
  var form = $('#add-group');

  if(form.length > 0){
    var defaultVal = form.find('.level-select');
    console.log(defaultVal);
  }

  $('#new-group').click(function(){
    console.log('clicked');

    var name = $('#group-name').val(),
        level = parseInt($('#group-level').val(),10),
        low = parseInt($('#group-hours-low').val(),10),
        high = parseInt($('#group-hours-high').val(),10),
        errors = false;

    console.log(name);
    console.log(level);
    console.log(low);
    console.log(high);

    if(name === ""){
        $('.error-container.name').text('Type cannot be empty');
        errors = true;
    }

    if(level < 1 || level > 5){
        $('.error-container.level').text('Invalid level');
        errors = true;
    }

    if(low < 0){
        $('.error-container.hours-low').text('Hours cannot be negative');
        errors = true;
    }

    if(high < 0){
        $('.error-container.hours-high').text('Hours cannot be negative');
        errors = true;
    }

    if(high < low){
        $('.error-container.hours-high').text('High hours must be less than low hours');
        errors = true;
    }

  });

});
